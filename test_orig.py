#%%
from dataclasses import asdict
from functools import partial
import json
from scan_orig import scan_dir
from typing import Iterator

#%%
# create Directory Object
root = scan_dir("/home/unkwn1/Downloads", file_hash=True)

#%%
# Dump snapshot to file
with open("output_test.json", "w") as f:
    data = json.dumps(asdict(root))


#%% 

def createIter(manifest) -> Iterator:
    root = manifest.subDirs
    tree = [{"files": manifest.files, "subDirs": root}]
    for d in root:
        tree.append({
            "files": d.files,
            "subDirs": d.subDirs
                    })
    return iter(tree)

#%%
i = createIter(root)

#%%


#%%
# setup variables
tree = []
depth = 3
level = 0
tree_dict = 
tree.append([root])
#%%

while level <= depth and level < len(tree):    
    parent = tree[level]   
    level +=1
    dir_level = []
    for directory in parent:
       [dir_level.append(scan_dir(d, level)) for d in directory.subDirs]
    tree.append(dir_level)

#%%


# %%
with open("output_test.json", "r") as f:
    data = json.loads(f.read())

#%% 

data
# %%
dirKey = "subDirs"
root = data[dirKey]
tree = [{"files": data["files"], "subDirs": root}]
#%%
for d in root:
    tree.append({
        "files": d.get("files"),
        "subDirs": d.get("subDirs")
                })
# %%
tree[0]
# %%
tree[0].keys()
# %%
tree_iter = iter(tree)
# %%
t = next(tree_iter)
# %%
t.keys()
# %%
