#%%
from functools import partial
import json
from scan import scan_dir

#%%
root = scan_dir("/home/unkwn1/Downloads", file_hash=True)

#%% 
tree = []
depth = 3
level = 0
tree.append([root])
#%%

while level <= depth and level < len(tree):    
    parent = tree[level]   
    level +=1
    dir_level = []
    for directory in parent:
       [dir_level.append(scan_dir(d, level)) for d in directory.subDirs]
    tree.append(dir_level)

#%%


# %%
with open("output_test.json", "r") as f:
    data = json.loads(f.read())

#%% 

data
# %%
dirKey = "subDirs"
root = data[dirKey]
tree = [{"files": data["files"], "subDirs": root}]
#%%
for d in root:
    tree.append({
        "files": d.get("files"),
        "subDirs": d.get("subDirs")
                })
# %%
tree[0]
# %%
tree[0].keys()
# %%
tree_iter = iter(tree)
# %%
t = next(tree_iter)
# %%
t.keys()
# %%
