#%%
from distutils import dir_util
from multiprocessing.pool import ThreadPool
import os
import pickle
from dataclasses import dataclass, replace, asdict, field
import subprocess
import json
from concurrent.futures import ThreadPoolExecutor, as_completed
from typing import Iterator

# %%

@dataclass(frozen=True)
class File:
    """ File() stores records of a scanned file in a Directory.

        generate_hash()
          - a static method that generates an md5 hash for a file.
          - File() is frozen hash must be supplied during init (see scan_dir() File call)

          Args:
            - f:str - path to a file to md5 hash
    """
    name: str
    path: str       # full path
    size: int       # add repr to do kb conversion - round(stat.st_size/(1024**2),3)
    owner: str      # uid/gid
    hash: str = ""
    @staticmethod
    def generate_hash(f:str) -> str:
        fout = subprocess.run(["md5sum", f], capture_output=True)
        return fout.stdout.decode().rstrip().split(" ")[0]
    
@dataclass(unsafe_hash=True, repr=True, init=True)
class Directory:
    name: str
    path: str
    size: int               # sum of files + sub directory size
    level: int
    depth: int
    subDirs: list = field(default_factory=list, hash=False)
    files: list[File] = field(default_factory=list, hash=False)
    def __post_init__(self):   
        if self.depth == 0 or self.level <= self.depth:
            level = self.level + 1
            size = [self.size]
            dir_objs = [scan_dir(d, level) for d in self.subDirs]
            self.subDirs = dir_objs
            [size.append(d.size) for d in self.subDirs]
            self.size = sum(size)
            self.level = level

    def __iter__(self) -> Iterator:
        tree = [{"files": self.files, "subDirs": self.subDirs}]
        for d in self.subDirs:
            tree.append({
                "files": d.files,
                "subDirs": d.subDirs
                        })
        return iter(tree)

    def __repr__(self) -> str:
        return json.dumps(asdict(self), indent=4)
        
def scan_dir(fp:str, level:int = 0, depth:int=0, file_hash:bool = True, file_stats: bool = False) -> Directory:
    """Takes a directory path and returns a nested  Directory object.
        Args:
          - fp:str - the absolute path to the source directory
          - depth:int - limits the depth of a scan
          - level:int - defaults to zero and is auto set. Leave it alone\n
        Return:
          - Directory() - nested, subdirs will be processed from strings to Directory()'s

        TODO: Break this up, subdir loop has to be here to catch level/depth param's
    """
    files = []
    files_size = []
    subDirs = []
    for item in os.scandir(fp):
        if item.is_file():
            fstat = item.stat()
            files_size.append(fstat.st_size)
            if file_hash == False:
                files.append(
                    File(
                    name=item.name, path=item.path,size=fstat.st_size,
                    owner=f"{fstat.st_uid}/{fstat.st_gid}",
                    )
                )         
            else:
                files.append(
                    File(
                    name=item.name, path=item.path,size=fstat.st_size,
                    owner=f"{fstat.st_uid}/{fstat.st_gid}",
                    hash=File.generate_hash(item.path),
                    )
                )
        elif item.is_dir():
            subDirs.append(item.path)
        else:
            continue

        # TODO: check for depth / level and if subDirs not empty.
    return Directory(
        name=fp.split("/")[-1], path=fp, subDirs=subDirs,
        files=files, level=level, depth=depth, size=sum(files_size)
        )

# %%

#test = scan_dir("/home/unkwn1/Downloads", file_hash=True)
#print(test)

"""
with open("output_test.json", "wb") as f:
    f.write(pickle.dumps(test))
#print(json.dumps(asdict(test), indent=4))
with open("output_test.json", "rb") as f:
    print(hash(pickle.load(f)))
    """